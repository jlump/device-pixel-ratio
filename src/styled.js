import styled from 'styled-components'

export default styled.div`
    .content-container {
        display: flex;
        justify-content: space-between;
        width: 100vw;
        height: 95vh;
        .btn-container {
            display: flex;
            align-items: center;
            .create-btn {
                padding: 10px;
                background-color: #ffffff0f;
                border-radius: 20px;
                cursor: pointer;
                transition: 0.5s ease;
                &:hover {
                    background-color: #ffffffbf;
                    color: black;
                }
            }
        }
        .frame {
            background-color: #ffffffbf;
            margin: 20px 40px 40px 40px;
            border-radius: 20px;
        }
        
        .target-div {
            width: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            .target {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                width: 300px;
                height: 500px;
                background-color: yellow;
                .image-container {
                    img {
                        width: 100px;
                        height: 100px;
                    }
                }
                .body-container {
                    color: black;
                    font-size: 32px;
                }
            }
        }

        .output-container {
            width: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            canvas {
                margin: 0 1px;
            }
        }
    }
`