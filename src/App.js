import html2canvas from 'html2canvas'
import './App.css';
import AppStyled from './styled'
import exampleImage from './assets/example.jpg'
import { styled } from 'styled-components';
import { BackgroundColor } from 'chalk';
import { useEffect, useState } from 'react';
import { useWindowSize } from './utils/hook/useWindowSize/index';

const App = () => {

  const [pixelRatioSize, setPixelRatioSize] = useState(window.devicePixelRatio)

  const windowSize = useWindowSize()

  useEffect(() => {
    setPixelRatioSize(window.devicePixelRatio)
  }, [windowSize])

  const createCanvas = () => {
    const targetElement = document.getElementById('target')
    const imageElement = document.getElementById('img')
    const textElement = document.getElementById('body')
    // targetElement.style.width = `${600/window.devicePixelRatio}px`;
    // targetElement.style.height = `${1000/window.devicePixelRatio}px`;
    // imageElement.style.width = `${200/window.devicePixelRatio}px`;
    // imageElement.style.height = `${200/window.devicePixelRatio}px`;
    // textElement.style.fontSize = `${64/window.devicePixelRatio}px`;
    html2canvas(targetElement).then(function (canvas) {
      const ctx = canvas.getContext('2d')
      const width = canvas.width
      const height = canvas.height

      canvas.style.width = '300px';
      canvas.style.height = '500px';

      // canvas.style.width = width + "px";
      // canvas.style.height = height + "px";

      console.log(width, height)
      document.getElementById('output-container').appendChild(canvas);
    });
    // targetElement.style.width = '300px';
    // targetElement.style.height = '500px';
    // imageElement.style.width = '100px'
    // imageElement.style.height = '100px'
    // textElement.style.fontSize = '32px'
  }

  const createCanvas2 = () => {
    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');

    // Set display size (css pixels).
    var size = 200;
    canvas.style.width = size + "px";
    canvas.style.height = size + "px";

    // Set actual size in memory (scaled to account for extra pixel density).
    var scale = window.devicePixelRatio; // Change to 1 on retina screens to see blurry canvas.
    canvas.width = size * scale;
    canvas.height = size * scale;

    // Normalize coordinate system to use css pixels.
    ctx.scale(scale, scale);

    ctx.fillStyle = "#bada55";
    ctx.fillRect(10, 10, 300, 300);
    ctx.fillStyle = "#ffffff";
    ctx.font = '18px Arial';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';

    var x = size / 2;
    var y = size / 2;

    var textString = "I love MDN";
    ctx.fillText(textString, x, y);
  }

  const handleClickBtn = () => {
    createCanvas()
  }

  return (
    <div className="App" id="App">
      <header className="App-header">
        <div className="window-device-pixel-ratio">
          devicePixelRatio : {pixelRatioSize}
        </div>
        <AppStyled>
          <div className="content-container">
            <div className="target-div frame">
              <div className="target" id="target">
                <div className="image-container">
                  <img id="img" src={exampleImage} />
                </div>
                <div className="body-container" id="body">
                  devicePixelRatio
                </div>
              </div>
            </div>
            <div className="btn-container">
              <div className="create-btn" onClick={handleClickBtn}>
                Create
              </div>
            </div>
            <div className="output-container frame" id="output-container">
              {/* <canvas id="canvas"></canvas> */}
            </div>
          </div>
        </AppStyled>
      </header>
    </div>
  );
}

export default App;
